<?php

use Illuminate\Database\Seeder;

class ItemModuleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * Clear the table of data
         */
        DB::table('item_module')->truncate();

        $modules = [
            1,
            2,
            3,
            4,
            5,
            6,
            7,
            8,
            9,
            10
        ];

        foreach($modules as $moduleID) {

            for($i = 1; $i < 6; $i++) {

                /**
                 * Add database entry for new course assigning course name, random course code and random course leader
                 */
                DB::table('item_module')->insert([
                    'module_id' => $moduleID,
                    'item_id' => $i,
                    'complete' => 0
                ]);

            }

            $items = [
                6,
                7,
                8,
                9,
                10,
                11,
                12,
                13,
                14,
                15,
                16,
                17,
                18,
                19,
                20,
                21,
                22,
                23,
                24,
                25,
                26,
                27,
                28,
                29,
                30,
                31,
                32,
                33,
                34,
                35,
                36,
                37,
                38,
                39,
                40
            ];

            $itemCount = rand(2, 11);

            /**
             * Loop though courses and create a course with random data for each course
             */
            for($i = 1; $i < $itemCount; $i++) {

                /**
                 * Create new randomized array to shuffle users
                 */
                $itemNumber = array_rand($items);

                /**
                 * Add database entry for new course assigning course name, random course code and random course leader
                 */
                DB::table('item_module')->insert([
                    'module_id' => $moduleID,
                    'item_id' => $items[$itemNumber],
                    'complete' => 0
                ]);

                unset($items[$itemNumber]);

            }

        }
    }
}
