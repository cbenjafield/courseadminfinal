<?php

use Illuminate\Database\Seeder;

class ModuleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * Clear the table of data
         */
        DB::table('modules')->truncate();

        $users = [
            1,
            2,
            3,
            4,
            5,
            6,
            7,
            8,
            9,
            10
        ];

        /**
         * Loop though courses and create a course with random data for each course
         */
        for($i = 1; $i < 11; $i++) {

            /**
             * Create new randomized array to shuffle users
             */
            $userNumber = array_rand($users);

            /**
             * Add database entry for new course assigning course name, random course code and random course leader
             */
            DB::table('modules')->insert([
                'title' => 'Module ' . $i,
                'code' => 'CIS' . rand(1000, 4999),
                'leader' => $users[$userNumber]
            ]);

        }
    }
}
