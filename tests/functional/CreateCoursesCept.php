<?php 
$I = new FunctionalTester($scenario);
$I->am('a Admin');
$I->wantTo('create a new course');

// Log in as user
Auth::loginUsingId(11);
$I->seeAuthentication();

// When
$I->amOnPage('/admin/dash');
//  And
$I->click('Add a new course');
// Then
$I->seeCurrentUrlEquals('/admin/courses/create');
$I->see('Add New Course', 'h1');

// When
$I->SubmitForm(
    '#form',
    [
        'title' => 'newcourse',
        'code' => 'CIS0011',
        'courseleader' => '12'
    ],
    'submit'
);

// Then
$I->seeCurrentUrlMatches('~/admin/courses/(\d+)~');
$I->seeRecord('courses', ['title' => 'newcourse']);
$I->see('newcourse', 'h1');



