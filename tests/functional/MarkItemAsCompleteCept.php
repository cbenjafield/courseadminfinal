<?php
$I = new FunctionalTester($scenario);
$I->am('a God Admin');
$I->wantTo('mark an item as complete');

// Log in as user
// When
Auth::loginUsingId(11);
$I->seeAuthentication();
// Then
$I->amOnPage('/admin/modules/2');
$I->see('Module 2', 'h1');
$I->see('Ad atque voluptatum repellendus enim');
$I->see('29markcomplete');
$I->dontSee('29markuncomplete');
$I->see('Dolor nesciunt id recusandae cupiditate omnis.');
$I->see('1markcomplete');
$I->dontSee('1markuncomplete');

// When
$I->click('29markcomplete');
// Then
$I->amOnPage('/admin/modules/2');
// And
$I->see('29markuncomplete');
$I->dontSee('29markcomplete');
