<?php
$I = new FunctionalTester($scenario);
$I->am('a Admin');
$I->wantTo('delete add a module to a course association');

// Log in as user
Auth::loginUsingId(11);
$I->seeAuthentication();

// Then
$I->amOnPage('/admin/courses/2/edit');
// And
$I->see('Software & Systems', 'h1');
$I->see('Module 1');
$I->see('Module 2');
$I->see('Module 7');
$I->see('Module 8');
$I->see('Module 9');
$I->see('Module 10');
// Then
$I->uncheckOption('form input[value="7"]');
// And
$I->click('Update Course');
// Then
$I->amOnPage('/admin/courses/2');
// And
// $I->see('Your course has been updated!');
$I->dontSee('Module 7');