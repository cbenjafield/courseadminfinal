<?php
$I = new FunctionalTester($scenario);
$I->am('a God Admin');
$I->wantTo('delete an item');

// Log in as user
// When
Auth::loginUsingId(11);
$I->seeAuthentication();

// Then Create some courses...
$I->haveRecord('items', [
    'id' => '43',
    'text' => 'a test item',
    'default' => 1,
]);

// When
$I->amOnPage('/admin/items');
// And
$I->see('a test item', 'p');
$I->seeRecord('items', [
        'text' => 'a test item']
);
// Then
$I->see('delete43');
//$I->seeElement('a', ['name' => '41delete']);
$I->click('delete43');
// Then
$I->seeCurrentUrlEquals('/admin/items');
$I->dontSee('a test item', 'p');