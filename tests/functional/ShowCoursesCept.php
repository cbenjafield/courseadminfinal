<?php
$I = new FunctionalTester($scenario);
$I->am('a God Admin');
$I->wantTo('see the list of courses');

// Log in as user
// When
Auth::loginUsingId(11);
$I->seeAuthentication();

// Then Create some courses...
$I->haveRecord('courses', [
    'id' => 5,
    'title' => 'Web Design and Development',
    'code' => 'BIS11111111',
    'leader' => 1,
]);
$I->haveRecord('courses', [
    'id' => 6,
    'title' => 'Web Systems Development',
    'code' => 'BIS22222222',
    'leader' => 8,
]);
// Then
$I->seeRecord('courses', ['title' => 'Web Design and Development']);
$I->amOnPage('/admin/courses');
$I->see('Courses', 'h1');
$I->see('Web Design and Development', 'a.item');
$I->see('Web Systems Development', 'a.item');
$I->see('Web', 'a.item');
$I->see('Application Development', 'a.item');
$I->click('Web Design and Development');
$I->seeCurrentUrlEquals('/admin/courses/5');
$I->see('Web Design and Development', 'h1');