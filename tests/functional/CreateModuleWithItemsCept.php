<?php
$I = new FunctionalTester($scenario);
$I->am('a Admin');
$I->wantTo('create a new module');

// Log in as user
Auth::loginUsingId(11);
$I->seeAuthentication();

// When
$I->amOnPage('/admin/dash');
//  And
$I->click('Add a new module');
// Then
$I->seeCurrentUrlEquals('/admin/modules/create');
$I->see('Add New Module', 'h1');

// When
$I->SubmitForm(
    '#form',
    [
        'title' => 'newmodule',
        'code' => 'CIS9999',
        'moduleleader' => '11',
        'itemModules' => [
            '6' => '6',
            '7' => '7'
        ]
    ],
    'submit'
);

// Then
$I->seeCurrentUrlMatches('~/admin/modules/(\d+)~');
$I->seeRecord('modules', ['title' => 'newmodule']);
$modid = $I->grabRecord('modules', ['title' => 'newmodule']);
$I->seeRecord('item_module', ['module_id' => $modid->id]);
$I->seeRecord('item_module', ['item_id' => '6']);
$I->seeRecord('item_module', ['module_id' => '1', 'item_id' => '1']);
$I->seeRecord('item_module', ['module_id' => $modid->id, 'item_id' => '6']);
$I->seeRecord('item_module', ['module_id' => $modid->id, 'item_id' => '7']);
$I->see('newmodule', 'h1');



