<?php
$I = new FunctionalTester($scenario);
$I->am('a Admin');
$I->wantTo('create a new item');

// Log in as user
Auth::loginUsingId(11);
$I->seeAuthentication();

// When
$I->amOnPage('/admin/dash');
//  And
$I->click('Add a new item');
// Then
$I->seeCurrentUrlEquals('/admin/items/create');
$I->see('Add New Item', 'h1');

// When
$I->SubmitForm(
    '#form',
    [
        'text' => 'newdefaultitem',
        'default' => 'checked'
    ],
    'submit'
);

// Then
$I->seeCurrentUrlEquals('/admin/items');
$I->seeRecord('items', ['text' => 'newdefaultitem']);
$I->see('newdefaultitem');



