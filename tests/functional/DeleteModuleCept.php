<?php
$I = new FunctionalTester($scenario);
$I->am('a God Admin');
$I->wantTo('delete a module');

// Log in as user
// When
Auth::loginUsingId(11);
$I->seeAuthentication();

// Then
$I->amOnPage('/admin/modules');
// And
$I->see('Module 2', 'a.item');
$I->seeRecord('modules', [
    'title' => 'Module 1']
);
// Then
$I->click('Module 2 delete');
// Then
$I->seeCurrentUrlEquals('/admin/modules');
$I->dontSee('Module 2', 'a.item');