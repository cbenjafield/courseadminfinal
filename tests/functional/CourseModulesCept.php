<?php 
$I = new FunctionalTester($scenario);
$I->am('a Admin');
$I->wantTo('check I see all modules linked to each course');

// Log in as user
Auth::loginUsingId(11);
$I->seeAuthentication();

// Then
$I->amOnPage('/admin/courses/2');

// And
$I->see('Software & Systems', 'h1');
$I->see('Module 1');
// And
$I->see('Module 2');