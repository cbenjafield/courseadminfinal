<?php
$I = new FunctionalTester($scenario);
$I->am('a Admin');
$I->wantTo('create a new module');

// Log in as user
Auth::loginUsingId(11);
$I->seeAuthentication();

// When
$I->amOnPage('/admin/dash');
//  And
$I->click('Add a new module');
// Then
$I->seeCurrentUrlEquals('/admin/modules/create');
$I->see('Add New Module', 'h1');

// When
$I->SubmitForm(
    '#form',
    [
        'title' => 'newmodule',
        'code' => 'CIS9999',
        'moduleleader' => '11'
    ],
    'submit'
);

// Then
$I->seeCurrentUrlMatches('~/admin/modules/(\d+)~');
$I->seeRecord('modules', ['title' => 'newmodule']);
$I->see('newmodule', 'h1');



