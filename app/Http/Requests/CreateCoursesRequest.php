<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Auth;
use Illuminate\Support\Facades\Redirect;

class CreateCoursesRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // set to true so anyone can edit this
        //return true;

        // check that only admins can access the pages that should be secure.
        if (in_array($this->route()->getName(), ['admin.courses.create', 'admin.courses.store', 'admin.courses.edit', 'admin.courses.update', 'admin.courses.destroy'])) {
            if (Auth::check() && Auth::user()->admin == '1') {
                // allow to see the pages
                return true;
            } else {
                // return false and be forwarded to the 403 error page
                return false;
            }
        } else {
            return true;
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (in_array($this->route()->getName(), ['admin.courses.store', 'admin.courses.update'])) {
            return [
                'title' => 'required',
                'code' => 'required',
                'courseleader' => 'required'
            ];
        } else {
            return [];
        }
    }
}
