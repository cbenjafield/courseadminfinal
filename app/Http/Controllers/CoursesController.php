<?php

namespace App\Http\Controllers;
use App\User;
use DB;
use App\Course;
use Illuminate\Http\Request;
use Redirect;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;

class CoursesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
    */
    public function index(Requests\CreateCoursesRequest $request)
    {
        // return a list of all courses
        $allCourses = Course::all();
        return view('admin.courses.list', ['allCourses' => $allCourses]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(Requests\CreateCoursesRequest $request)
    {
        $users = User::where('admin', 0)
            ->orderBy('name', 'desc')
            ->get();

        $moduleCourseAssociations = Course::moduleCourseAssociations(0);

        return view('admin.courses.create', ['users' => $users])->with(['moduleCourseAssociations' => $moduleCourseAssociations]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Requests\CreateCoursesRequest  $request
     * @return Response
     */
    public function store(Requests\CreateCoursesRequest $request)
    {
        $course = new Course;

        $course->title = $request->title;
        $course->code   = $request->code;
        $course->leader = $request->courseleader;

        $course->save();

        $courseModules = $request->get('courseModules');

        // write module associations
        if (count($courseModules) > 0 ) {
            foreach ($courseModules as $module_id) {
                DB::table('course_module')->insert([
                    'course_id' => $course->id, 'module_id' => $module_id
                ]);
            }
        }

        return Redirect::route('admin.courses.show', [$course]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show(Requests\CreateCoursesRequest $request, $id)
    {
        $course = Course::findORFail($id);
        $courseleader = User::find($course->leader);
        return view('admin.courses.show', ['course' => $course])->with(['courseleader' => $courseleader]);


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit(Requests\CreateCoursesRequest $request, $id)
    {
        $users = User::where('admin', 0)
            ->orderBy('name', 'desc')
            ->get();
        $userselector = array();
        foreach($users as $user) {
            $userselector[$user->id] = $user->name;
        };
        $course = Course::findOrFail($id);
        // call the coursemoduleassociations method from the courses model than chain this to the return to the view.
        $moduleCourseAssociations = Course::moduleCourseAssociations($id);

        // pass over two arrays/variables to the view.
        return view('admin.courses.edit', ['course' => $course])->with(['userselector' => $userselector])->with(['moduleCourseAssociations' => $moduleCourseAssociations]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */

    public function update(Requests\CreateCoursesRequest $request, $id)
    {
        $course = Course::findORFail($id);

        $course->update([


            'title' => $request->get('title'),
            'code' => $request->get('code'),
            'leader' => $request->get('courseleader')
        ]);

        $moduleCourseAssociations = Course::moduleCourseAssociations($id);
        $courseModules = $request->get('courseModules');
        foreach ($moduleCourseAssociations as $module)
        {
            $skipped = false;
            // error capture for having no modules associated
            if (count($courseModules) > 0 ) {
                // check if association already exists if it does remove it from the $courseModules array then remove it frm the db
                if ($module->checked == 1 && (($key = array_search($module->id, $courseModules)) !== false)) {
                    unset($courseModules[$key]);
                    $skipped = true;
                }
            }
            // delete any removed associations
            if($module->checked == 1 && !$skipped)
            {
                DB::table('course_module')->where('course_id', '=', $course->id)->where('module_id', '=', $module->id)->delete();
            }
            unset($key);
        }
        // write new associations
        if (count($courseModules) > 0 ) {
            foreach ($courseModules as $module_id) {
                DB::table('course_module')->insert([
                    'course_id' => $course->id, 'module_id' => $module_id
                ]);
            }
        }

        return Redirect::route('admin.courses.show', $course->id)->with('message', 'Your course has been updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy(Requests\CreateCoursesRequest $request, $id)
    {
        // delete
        Course::destroy($id);

        // redirect
        //Session::flash('message', 'Successfully deleted the Course!');
        return \Redirect::route('admin.courses.index');

    }
}
