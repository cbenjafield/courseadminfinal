<?php

namespace App\Http\Controllers;

use App\Item;
use App\Module;
use App\ItemModule;
use Illuminate\Http\Request;
use Redirect;
use Carbon;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ItemModuleController extends Controller
{

    public function index()
    {
        //
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {

        /*$module_id = $request->get('module_id');

        $itemmodule = ItemModule::findORFail($id);

        $itemmodule->update([
            'completed' => $request->get('completed')
        ]);*/

        $GLOBALS ['module_id'] = $request->get('module_id');
        $GLOBALS ['item_id'] = $request->get('item_id');
        $module_id = $request->get('module_id');
        $mytime = Carbon\Carbon::now();

        $itemModule = DB::table('item_module')->where('module_id', $GLOBALS['module_id'])->where('item_id', $GLOBALS['item_id'])->update(['complete' => 1, 'updated_at' => $mytime]);


        //dd($itemModule);

        return redirect()->to('/admin/modules/'.$module_id)->with('message', 'Your item has been updated!');
    }

    public function destroy($id)
    {
        //
    }

}
