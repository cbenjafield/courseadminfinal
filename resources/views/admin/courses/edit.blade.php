@extends('layouts.master')

@section('title')
    Edit - {{ $course->title }}
@stop

@section('content')

    <h1 class="small-12 columns">Edit - {{ $course->title }}</h1>

    @if (count($errors) > 0)
        <div class="alert-box alert large-12 columns">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    {!! Form::model($course, array('method' => 'put', 'route' => ['admin.courses.update', $course->id], 'data-abide' => '')) !!}
        <!-- Title Form Input-->
        <div class="large-12 columns">
            {!! Form::label('title:', 'Course title:') !!}
            {!! Form::text('title', Input::old('title'), array('placeholder'=>'Course Title', 'required' => '')) !!}
        </div>

        <!-- code Form Input-->
        <div class="large-12 columns">
            {!! Form::label('code', 'Course code:') !!}
            {!! Form::text('code', Input::old('code'), array('placeholder'=>'Course Code', 'required' => '')) !!}
        </div>
        <!-- Leader Form Dropdown-->
        <div class="large-12 columns">
            {!! Form::label('courseleader', 'Leader:') !!}
            <!-- moved the logic to the controller -->
            {!! Form::select('courseleader', $userselector, $course->leader) !!}
        </div>
        <!-- add module associtation checkboxes partial blade -->
        <div class="row large-12 columns ">
            <h2>Module Associations</h2>
                @if ( count($moduleCourseAssociations) > 0)
                    @include('admin.courses.courseModuleAssociations')
                @endif
        </div>
        <!-- Course submit button -->
        <div class="large-12 columns">
            {!! Form::submit('Update Course', array('class'=>'button right')) !!}
        </div>
        {!! csrf_field() !!}
    {!! Form::close() !!}
@stop
