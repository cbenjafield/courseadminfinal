@extends('layouts.master')
@section('title')
    Course - {{ $course->code }} - {{ $course->title }}
@stop
@section('content')

    @if ( Session::get('message'))
        <div class="alert-box warning large-12 columns">
            {{ Session::get('message') }}
        </div>
    @endif
    <h1 class="small-12 columns">{{ $course->title }}</h1>
    <div class="row small-12 columns">
        <p><strong>Course Code:</strong> {{ $course->code }}</p>
        <p><strong>Course Leader:</strong> {{ $courseleader->name }}</p>
        <div>
            <h2>Associated Modules</h2>
            @if ( !$course->modules->count() )
                Your course has no Modules linked.
            @else
                <ul class="no-bullet small-12 columns">
                    @foreach( $course->modules as $module )
                        <li><a href="{{ route('admin.modules.show', [$module->id]) }}">{{ $module->title }} - {{ $module->code }}</a></li>
                    @endforeach
                </ul>
            @endif
        </div>
        <a href="{{ route('admin.courses.edit', $course->id) }}" class="button small warning right">Edit Course</a>

    </div>

@stop