@extends('layouts.master')

@section('title', 'Add a New Course')
@stop

@section('content')

    <h1 class="small-12 columns">Add new Course</h1>

        @if (count($errors) > 0)
            <div class="alert-box alert large-12 columns">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

    <form method="POST" action="{{ url('admin/courses') }}" enctype="multipart/form-data" id="form" data-abide>
        <!-- Title Form Input-->
        <div class="large-12 columns">
            <label for="Title">Title:</label>
            <input type="text" id="title" name="title" placeholder="Course Title" value="{{ Input::old('title') }}" required >
            <small class="error">A title is required.</small>
        </div>
        <!-- code Form Input-->
        <div class="large-12 columns">
            <label for="code">Course Code:</label>
            <input type="text" id="code" name="code" placeholder="Course Code" value="{{ Input::old('code') }}" required >
            <small class="error">A course code is required. example BIS123456789</small>
        </div>
        <!-- Leader Form Dropdown-->
        <div class="large-12 columns">
            <label for="courseleader">Leader:</label>
            <select id="cousrseleader" name="courseleader">
                @foreach($users as $user)
                    <option value="{{ $user->id }}">{{ $user->name }}</option>
                @endforeach
            </select>
        </div>
        <!-- add module associtation checkboxes partial blade -->
        <div class="row large-12 columns ">
            <h2>Module Associations</h2>
            @if ( count($moduleCourseAssociations) > 0)
                @include('admin.courses.courseModuleAssociations')
            @endif
        </div>
        <!-- Course submit button -->
        <div class="large-12 columns">
            <button id="submit" name="submit" class="button right">Create Course</button>
        </div>
        {!! csrf_field() !!}
    </form>

@stop