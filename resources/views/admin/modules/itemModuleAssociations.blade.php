<?php
    $defaultCount = 0;
    $optionalCount = 0;
?>

@foreach($itemModuleAssociations as $item)
    @if ($item->default == 0)
         <?php  $optionalCount ++ ?>
    @endif
    @if ($item->default == 1)
        <?php $defaultCount ++ ?>
    @endif
    @if ($defaultCount == 1)
        <h2>Default Items</h2>

    @endif
    @if ($optionalCount == 1)
        <hr />
        <h2>Optional Items</h2>

    @endif

    <div class="small-12 columns">
        @if ($item->default == 0)
            {!! Form::checkbox('itemModules['.$item->id.']', $item->id, $item->checked ) !!}
        @else
            {!! Form::hidden('itemModules['.$item->id.']', $item->id) !!}
        @endif
            {!! Form::label('itemModules['.$item->id.']', $item->text) !!}
    </div>
@endforeach