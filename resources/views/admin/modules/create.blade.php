@extends('layouts.master')

@section('title', 'Add a New Module')
@stop

@section('content')

    <h1 class="small-12 columns">Add new Module</h1>

    @if (count($errors) > 0)
        <div class="alert-box alert small-12 columns">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form method="POST" action="{{url('admin/modules') }}" enctype="multipart/form-data" id="form" data-abide>
        <!-- Title Form Input-->
        <div class="small-12 columns">
            <label for="Title">Title:</label>
            <input type="text" id="title" name="title" placeholder="Module Title" value="{{ Input::old('title') }}" required >
            <small class="error">A title is required.</small>
        </div>
        <!-- code Form Input-->
        <div class="small-12 columns">
            <label for="code">Module Code:</label>
            <input type="text" id="code" name="code" placeholder="Module Code" value="{{ Input::old('code') }}" required>
            <small class="error">A module code is required. example CIS1234</small>
        </div>
        <!-- Leader Form Dropdown-->
        <div class="small-12 columns">
            <label for="moduleleader">Leader:</label>
            <select id="moduleleader" name="moduleleader">
                @foreach($users as $user)
                    <option value="{{ $user->id }}">{{ $user->name }}</option>
                @endforeach
            </select>
        </div>
        <!-- add item association checkboxes partial blade -->
        <div class="row large-12 columns ">
            @if ( count($itemModuleAssociations) > 0)
                @include('admin.modules.itemModuleAssociations')
            @endif
        </div>
        <!-- Course submit button -->
        <div class="small-12 columns">
            <button id="submit" name="submit" class="button right">Create Module</button>
        </div>
        {!! csrf_field() !!}
    </form>

@stop

