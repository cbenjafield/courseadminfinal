@extends('layouts.master')

@section('title')
    Edit - {{ $item->text }}
@stop

@section('content')

    <h1 class="small-12 columns">Edit - {{ $item->text }}</h1>

    @if (count($errors) > 0)
        <div class="alert-box alert large-12 columns">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif

        {!! Form::model($item, array('method' => 'put', 'route' => ['admin.items.update', $item->id], 'data-abide' => '')) !!}
                <!-- Text Form Input-->
        <div class="large-12 columns">
            {!! Form::label('text:', 'Item Text:') !!}
            {!! Form::text('text', Input::old('text'), array('placeholder'=>'Item Text', 'required' => '')) !!}
        </div>

        <!-- default Form Input-->
        <div class="large-12 columns">
            {!! Form::label('default', 'Default Item:') !!}
            {!! Form::checkbox('default', 1, $item->default) !!}
        </div>


        <!-- Item submit button -->
        <div class="large-12 columns">
            {!! Form::submit('Update Item', array('class'=>'button right')) !!}
        </div>
        {!! csrf_field() !!}
        {!! Form::close() !!}
@stop