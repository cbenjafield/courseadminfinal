@extends('layouts.master')
@section('title')
    403 Forbidden
@stop
@section('content')

    @if ( Session::get('message'))
        <div class="alert-box warning large-12 columns">
            {{ Session::get('message') }}
        </div>
    @endif

    <h1>403 Forbidden</h1>
    <div class="large-12 columns">
        <p class="warning">You are not authorised to view that page or carry out that action!</p>
        <p>If you think you should have access please contact the department administrator!</p>
    </div>
@stop