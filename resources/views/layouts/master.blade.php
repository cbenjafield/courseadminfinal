<!DOCTYPE html>
<html lang="en">
<link rel="stylesheet" href="{{url('css/app.css')}}" type="text/css"/>
<head>
    <title>Course Admin - @yield('title')</title>
</head>
<body>
<div class="container clearfix">
    @include('/layouts/header')
    @yield('content')
    @include('/layouts/footer')
</div>
</body>
</html>
